<?php

namespace App;

class WeatherMonitor
{
    protected $temperatureService = null;

    public function __construct($temperatureService)
    {
        $this->temperatureService = $temperatureService;
    }

    public function getAverageTemperature(string $start, string $end)
    {
        $startTemp = $this->temperatureService->getTemperature($start);
        $endTemp = $this->temperatureService->getTemperature($end);

        return ($startTemp + $endTemp) / 2;
    }
}