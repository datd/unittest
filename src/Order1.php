<?php

namespace App;

class Order1
{
    public $quantity;
    public $unitPrice;
    public $amount;

    public function __construct(int $quantity, float $unitPrice)
    {
        $this->quantity = $quantity;
        $this->unitPrice = $unitPrice;
        $this->amount = $quantity * $unitPrice;
    }

    public function process($paymentGateway)
    {
        $paymentGateway->charge($this->amount);
    }
}