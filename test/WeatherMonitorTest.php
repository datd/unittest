<?php

use App\Services\TemperatureService;
use App\WeatherMonitor;
use PHPUnit\Framework\TestCase;

class WeatherMonitorTest extends TestCase
{
    public function tearDown() : void
    {
        Mockery::close();
    }

    public function testCorrectAverageReturn()
    {
        $serviceMock = $this->createMock(TemperatureService::class); // Giả lập class
        $map = [ //Map để giả lập giá trị truyền vào và giá trị trả ra khi gọi hàm giả lập
            ["7:00", 20],
            ["8:00", 20]
        ];
        $serviceMock
            ->expects($this->exactly(2)) //Hàm getTemperature đc gọi 2 lần bên trong hàm của class cần test
            ->method('getTemperature') //Giả lập hàm
            ->will($this->returnValueMap($map)); //Set kết quả trả về cho hàm giả lập tương ứng với map

        $weather = new WeatherMonitor($serviceMock);
        $this->assertEquals(20, $weather->getAverageTemperature("7:00", "8:00"));
    }

    public function testCorrectAverageReturnWithMockery()
    {
        $serviceMock = Mockery::mock(TemperatureService::class);
        $serviceMock->shouldReceive('getTemperature')->once()->with('7:00')->andReturn(20);
        $serviceMock->shouldReceive('getTemperature')->once()->with('8:00')->andReturn(20);
        $weather = new WeatherMonitor($serviceMock);
        $this->assertEquals(20, $weather->getAverageTemperature("7:00", "8:00"));
    }
}