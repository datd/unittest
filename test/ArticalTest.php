<?php

use PHPUnit\Framework\TestCase;
use App\Artical;

class ArticalTest extends TestCase
{
    protected $artical = null;

    public function setUp() : void
    {
        $this->artical = new Artical;
    }

    public function testEmptyArticalTitle()
    {
        $this->assertEmpty($this->artical->title);
    }

    public function testSlugEmptyWithNoTitle()
    {
        $this->assertSame($this->artical->getSlug(), "");
    }

    public function testSlugIsUnderscore()
    {
        $this->artical->title = "hoc unittest";
        $this->assertEquals($this->artical->getSlug(), "hoc_unittest");
    }

    public function testSlugIsUnderscoreWithMultileSpace()
    {
        $this->artical->title = "hoc              unittest";
        $this->assertEquals($this->artical->getSlug(), "hoc_unittest");
    }

    public function testSlugDoesNotStartOrEndWithUnderscore()
    {
        $this->artical->title = " hoc unittest ";
        $this->assertEquals($this->artical->getSlug(), "hoc_unittest");
    }

    public function testSlugWithoutNoneWordCharacter()
    {
        $this->artical->title = "hoc! unittest!";
        $this->assertEquals($this->artical->getSlug(), "hoc_unittest");
    }

    public function titleProvider()
    {
        return [
            ['hoc unittest', 'hoc_unittest'],
            ['hoc  unittest', 'hoc_unittest'],
            [' hoc unittest', 'hoc_unittest'],
            ['hoc! unittest!', 'hoc_unittest']
        ];
    }

    /**
     * @dataProvider titleProvider
     * **/
    public function testSlug($title, $slug)
    {
        $this->artical->title = $title;
        $this->assertEquals($this->artical->getSlug(), $slug);
    }
}