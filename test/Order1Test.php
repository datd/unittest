<?php

use PHPUnit\Framework\TestCase;
use App\Order1;

class Order1Test extends TestCase
{
    public function tearDown() : void
    {
        Mockery::close();
    }

    public function testOrderIsProcessedUsingMock()
    {
        $order = new Order1(3, 2000);
        $this->assertEquals(6000, $order->amount);
        $paymentGatewayMock = Mockery::mock('PaymentGateway');
        $paymentGatewayMock->shouldReceive('charge')->once()->with(6000);
        $order->process($paymentGatewayMock);
    }

    public function testOrderIsProcessedUsingSpy()
    {
        $order = new Order1(3, 2000);
        $this->assertEquals(6000, $order->amount);
        $paymentGatewaySpy = Mockery::spy('PaymentGateway');
        $order->process($paymentGatewaySpy);
        $paymentGatewaySpy->shouldHaveReceived('charge')->once()->with(6000);
    }
}