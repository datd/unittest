============ Bài 1: Giới thiệu khóa học UnitTesting ============

============ Bài 2: Cài đặt composer với PHP vào máy ============
Do em dùng Window nên em bỏ qua bài 3,4 (Cài trên môi trường MacOS và Linux)
- Bước 1: Cài đặt PHP, em đã cài XAMPP nên đã tích hợp sẵn cài đặt PHP vào Window. Kiểm
tra phiên bản php dùng command: php -v. Em dùng php 8.1.4
- Bước 2: Tải file Composer theo đường dẫn : https://getcomposer.org/download/, ấn chọn tải file exe.
- Bước 3: Install composer vào Window, lưu ý là chọn đường dẫn trỏ tới file php trong thư mục Xampp/Php/Php.exe
- Bước 3: Tiếp tục ấn next cho tới khi hoàn tất.
- Bước 4: Kiểm tra composer cài thành công bằng cách gõ: composer. Nếu màn hình CMD hiển thị các
tùy chọn nghĩa là đã hoàn tất.

============ Bài 5: Tạo project và cài đặt PHPUnit ============
- Tạo project tên tùy chọn. Ví dụ là UnitTest.
- Dùng composer để cài đặt package PHPUnit bằng cách gõ lệnh:
composer require --dev phpunit/phpunit
- Tùy vô phiên bản php thì nó tự cài phiên bản package tương ứng. Của em nó cài phpunit 9.5.20

============ Bài 6: Tạo và chạy thử bài test đầu tiên ============

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function TestEqualsResult()
    {
        $this->assertEqual(2, 1+1);
    }
}

Chạy comand line: vendor/bin/phpunit Test.php

Kết quả:
OK(1 test, 1 assert): 1 hàm test, 1 gọi hàm assert

============ Bài 7: Test multiple assertion ============

function add($a, $b)
{
    return $a+$b;
}

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function TestEqualsResult()
    {
        $this->assertEqual(2, add(1, 1));
        $this->assertEqual(4, add(2, 2));
    }
}

- hàm TestEqualsResult là hàm dùng để kiểm tra hàm add() có trả về kết quả giống mong muốn không.

Kết quả:
OK(1 test, 2 assert): 1 hàm test, 2 gọi hàm assert


============ Bài 8: Test multiple assertion, kiểm trả trả về không chính xác ============

function add($a, $b)
{
    return $a+$b;
}

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function testAddDoesNotReturnIncorrectSum()
    {
        $this->assertNotEqual(3, add(1, 1));
    }
}

- Hàm testAddDoesNotReturnIncorrectSum là hàm test xem hàm add trả về kết quả sai không.

Kết quả:
OK(1 test, 1 assert): 1 hàm test, 1 gọi hàm assert

============ Bài 9: Test class ============

- Bước 1: Tạo 1 class User.
class User{
    public $firstName = null;
    public $lastName = null;

    public function __construct($firstName = null, $lastName = null)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getFullName()
    {
        return trim($this->firstName . " " .$this->lastName);
    }
}

- Bước 2: Tạo class để test class User. Đặt tên là TestUser.
class TestUser{
    public function testFullName()
    {
        $user = new User("Danh", "Đạt");
        $this->assertEqual("Danh Dat", $user->getFullName());
    }

    public function testDefaultEmpty()
    {
        $user = new User();
        $this->assertEqual("", $user->getFullName());
    }
}

- Hàm testFullName: mục địch là để kiểm trả fullName trả về có giống như mong muốn.

Kết quả:
OK(1 test, 1 assert): 1 hàm testFullName, 1 gọi hàm assert


============ Bài 10: Cách đặt tên hàm test ============

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function hasFirstName()
    {
        $user = new User();
        $user->firstName = "TEST";
        $this->assertEqual("TEST", $user->firstName);
    }
}


- Sau khi chạy vendor/bin/phpunit Test thì hàm hasFirstName không chạy do không có "test" ở
đầu tên hàm.
- Có thể đặt tên hàm theo kiểu testCarmelCase hoặc test_snake_case

============ Bài 11: Chạy test theo ý muốn ============
- Chạy class test: vendor/bin/phpunit THƯ_MỤC/TÊN_FILE.php
- Chạy nhiều file bên trong thư mục: vendor/bin/phpunit THƯ_MỤC/
- Chạy theo hàm chỉ định: vendor/bin/phpunit THƯ_MỤC/ --filter=tên_Hàm
- Show kết quả có màu: vendor/bin/phpunit THƯ_MỤC/ --color


============ Bài 13: Sử dụng autoload với composer ============

- Tạo thư mục ví dụ tên là src.
- Tạo các file class vào thư mục src
- Tạo autload vào file composer.json:
{
 "autoload": {
   "psr-4": {
      "App\\" : "src"
    }
  }
}

"App\\": Đường dẫn gốc namespace
"src" : Đường dẫn thư mục nơi chứa file cần gọi

- Đặt namespace App; vào các class cần test mục đích để tiện gọi vào file test. Ví dụ:

* File src/User.php
namespace App;
class User{
  public function run(){
     return "Hello World";
  }
}

* File test user
use App\User;
require 'vendor/autoload.php';
class TestUser{
  public function testRunHelloWorld(){
    $user = new User();
    $this->assertEqual("Hello World", $user->run());
  }
}
- Chạy phpunit --bootstrap='vendor/autoload.php' : chạy câu lệnh đăng ký require 'vendor/autoload' vào các file để chạy autoload
- Chạy câu lệnh composer dump-autoload


============ Bài 14, 15: Test Queue Class ============

Test queue:

- Tạo file demo queue:
namespace App;

class Queue
{
    public $items = [];
    public function push($item)
    {
        $this->items[] = $item;
    }
    public function pop()
    {
        return array_pop($this->items);
    }
    public function getCount()
    {
        return count($this->items);
    }
}

- Tạo file TestQueue để test file Queue:


use PHPUnit\Framework\TestCase;
use App\Queue;

class QueueTest extends TestCase
{
    protected $queueClass;

    //Hàm set up dùng để tạo dựng 1 đối tượng dùng chung trong class
    //Thay vì phải tạo new Class trong mỗi function
    public function setUp() : void
    {
        $this->queueClass = new Queue;
    }

    //Hàm tearDown để gỡ bỏ đối tượng dùng chung đó.
    public function tearDown() : void
    {
        unset($this->queueClass);
    }

    // Hàm test khởi tạo queue là rỗng
    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, $this->queueClass->getCount());
    }

    //Hàm test khi đẩy 1 item vào queue và sẽ trả về đếm queue là 1
    public function testAnItemIsAddedToTheQueue()
    {
        $this->queueClass->push("Green");
        $this->assertEquals(1, $this->queueClass->getCount());
    }

    //Hàm pop các item khỏi queue, sau khi pop các item thì queue sẽ trả về rỗng
    public function testPopIsSuccess()
    {
        $this->queueClass->push("green");
        $this->queueClass->pop();
        $this->assertEmpty($this->queueClass->items);
    }
}


============ Bài 16: Kiến thức về hàm setUp và tearDown ============

- Hàm setUp và tearDown được khai báo theo public hoặc protected
- Hàm setUp sẽ được chạy trước các hàm test bên trong testClass
- Hàm tearDown sẽ chạy sau khi chạy các hàm test. Hàm tearDown thường unset các
khai báo class. Hàm này thường ít dùng nhưng nên dùng trong khai báo nhiều class vào hàm setUp.
Mục đích là tránh bị leak memory


============ Bài 17: Easily add a new test method using the test fixture ============

- Test phần tử được xóa ở đầu của queue không

* File queue
namespace App;

class Queue
{
    public $items = [];

    public function push($item)
    {
        $this->items[] = $item;
    }

    public function pop()
    {
        return array_pop($this->items);
    }

    public function getCount()
    {
        return count($this->items);
    }
}

* File để test Queue.php
use PHPUnit\Framework\TestCase;
use App\Queue;

class QueueTest extends TestCase
{
    public function testAnItemRemovedFrontTheFrontOfTheQueue()
    {
        $this->queueClass->push('first');
        $this->queueClass->push('second');

        //Mong muốn hàm pop trong class phải là first
        $this->assertEquals('first', $this->queueClass->pop());
    }
}

- Kết quả trả về không như mong đợi

- Tiến hành fix bug vào file Queue.php
- Do hàm push bên trong file queue dồn các phần tử vào mảng ở vị trí cuối của mảng
và hàm pop vừa xóa và trả về phần tử cuối mảng => "first" != "second"
- Fix bug bằng cách sửa hàm array_pop => array_shift
- Hàm array_shift xóa phần tử ở đầu mảng


============ Bài 18: Share fixtures between tests for resource-intensive data ============
- Sử dụng hàm
+ setUpBeforeClass: Hàm này được chạy trước khi class Test được chạy.
+ tearDownAfterClass: Hàm này được chạy cuối cùng sau khi class được chạy.

- Hàm setUpBeforeClass thường được dùng để khởi tạo đối tượng dùng chung trong class
- Hàm tearDownAfterClass thường được dùng để hủy đối tượng, ngắt kết nối DB

Ví dụ

class TestQueue
{
    protected static $queueStatic = null;

    public static function setUpBeforeClass()
    {
        static::$queueStatic = new Queue();
    }

    public function tearDownAfterClass()
    {
        static::$queueStatic = null;
    }
}

============ Bài 19: Testing exception ============
Testing cùng với exception được ném ra sau khi lỗi

Ví dụ:

<?php

use App\QueueException;

class Queue
{
    protected $items = [];

    public function push($item)
    {
        if($this->getCount() == static::MAX_ITEMS){
            throw new QueueException("Queue đầy !");
        }
        $this->items[] = $item;
    }
}
class QueueTest
{
    //Test exception được ném ra sau khi queue đầy
    public function testExceptionDuocNemRaSauKhiQueueBiDay()
    {
        for($i = 0; $i < Queue::MAX_ITEMS; $i++){
            $this->queueNonStatic->push($i);
        }
        //Hàm expectException để check messages của hàm push khi queue đầy
        $this->expectException(QueueException::class);
        //Test messages queue đầy
        $this->expectExceptionMessage("Queue đầy !");
        $this->queueNonStatic->push("Phần tử khiến queue bị đầy");
    }
}

============ Bài 20: Sử dụng mock để test tác vụ tốn thời gian ============
- Vì các tác vụ tốn thời gian nên hàm test sẽ lâu cho ra kết quả test
- Cho nên dùng mock để test

- Cách dùng:
$mock = $this->createMock(ClassCanTest::class);
$mock->method('hamCanTest')->willReturn($KetQuaSeTraVe);
$res = $mock->hamCanTest($paramTruyenVao);
$this->assertEqual($ketQuaMongMuon, $res);


============ Bài 21: Kỹ thuật đuổi đối tượng ra khỏi class đang lệ thuộc ============
- Lý do: Khi test 1 class mà class đang sử dụng class khác. VD ở đây là class User sử dụng class Mailer
thì sau khi chạy UserTest thì việc delay của hàm gửi email của class Mailer là không tránh khỏi.
=> Sử dụng kỹ thuật inject object để truyền đối tượng Mailer vào class User

- Bước 1: Khởi tạo thuộc tính $Mailer
- Bước 2: Tạo phương thức setMailer để set đối tượng cho thuộc tính
class User {
    protected $mailer;

    public function setMailer(Mailer $mailer){
        $this->mailer = $mailer;
    }
}

- Bước 3: Tiến hành test
class TestUser{
    public function testNotify(){
        $user = new User(); // Khởi tạo đối tượng cần test
        $mockMailer = $this->createMock(Mailer::class); // Mock giả lập 1 class mailer
        $mockMailer->method('sendMail')->willReturn(true); // Gọi hàm sendMail => sẽ trả về true
        $user->setMailer($mockMailer); //Gọi tới hàm setMailer
        $user->email = "dat@mail.com"; //Set email cho thuộc tính cần gửi
        $this->assertTrue($user->notify("Xin chào !")); // Chạy assert để test kết quả mong muốn là true
    }
}

============ Bài 22: Cấu hình mock ============
expects($this->once()) //Gọi hàm sendMail 1 lần
expects($this->never()) //Không gọi hàm
method('sendMail') //Tên hàm cần mock
with($this->equalTo("dat@mail.com"), "Xin chào !") //Truyền params vào hàm
willReturn(true) // Kết quả sẽ trả về

============ Bài 23: Chạy code cùng mock ============
- Hàm mock sẽ trả về null và không chạy code bên trong
- Cách để chạy trực tiếp dòng code trong hàm được mock:
B1: Tạo mock
$mock = $this->getMockBuilder(ClassCanMock::class)->setMethods(null)->getMock();
B2: Gọi tới hàm thuộc class cần mock
$className = new ClassName();
$mailerMock->method('tenHam')->will($this->throwException(new Exception));
B3: Set class vào class cần test
$className->set($mock);
B4: Assert để test kết quả
$this->assertTrue($user->tenHamCanTest($paramTruyenVào));

============ Bài 24: Tạo mock giả lập class không tồn tại ============
- Mục đích: tạo mock giả lập 1 class với phương thức không tồn tại
- Các bước tạo giả lập:
B1: Tạo class giả lập mock cùng với method
$classMocked = $this->getMockBuilder('TênClassCầnGiảLập')->setMethods(['tênHàm1', 'tênHàm2])->getMock();
B2: Config cho hàm (vì mặc định hàm mock giả lập trả về null mình config nó về true false,...)
$classMocked->method('tênHàm')->willReturn(true);

B3: Xong rồi, tiến hành test assert, tạo đối tượng cần test
$testClass = new TestClass($classMocked); //Truyền class giả lập vô class cần test để set đối tượng bên trong class cần test đó.
B3: Test kết quả
$this->assert($ketQuaMongMuon, $testClass->hamCanTest);

============ Bài 25: Cài đặt mockery ============
- Test multiple function
- Sử dụng với mock object
- Sử dụng cùng PHPUnit
- Lợi ích: dễ sử dụng hơn mock của PHPUnit, cách dùng gọn hơn

Câu lệnh cài đặt:
composer require mockery/mockery --dev

============ Bài 26: Sử dụng mockery để làm giả đối tượng không tồn tại ============
B1: Tạo đối tượng mock
$mock = Mockery::mock('TênClassCầnMock');

B2: Config để mock
$mock->shouldReceive('hamGiaLap') //Sẽ trả về hamGiaLap
    ->once()
    ->with(200) //Truyền vào hàm hamGiaLap
    ->andReturn(true); //Sẽ trả về true

B3: Đem $mock truyền vào Class cần test
$classCanTest->setClass($mock);
$this->assert($ketQuaMongMuon, $KetQuaSeTraVe);


============ Bài 27: Giả lập một class và hàm để test cùng với map ============

=>> Cách 1: Dùng mock có sẵn của UnitTest

- Mục đích: Khi gọi hàm giả lập nhìu lần, mình gọi hàm giả lập 2 lần với kết quả truyền vào và trả ra khác testExceptionDuocNemRaSauKhiQueueBiDay
- Cách dùng:
Bước 1: Tạo ra class mock giả lập
$classMocker = $this->createMock(ClassMock::class);

Bước 2: Tạo ra mảng map (map là mảng với giá trị đầu là giá trị truyền vào, giá trị sau là trả về)
$map = [
    [giáTriTruyenVaoHam, giaTriSeTraVe],
    [giáTriTruyenVaoHam, giaTriSeTraVe],
    ...
]

Bước 3: Cấu hình cho class vừa giả lập
$classMocker
    ->expects($this->exactly(2)) //Hàm hamCanGiaLap được gọi 2 lần bên trong hàm của class cần test
    ->method('hamCanGiaLap') //Giả lập hàm
    ->will($this->returnValueMap($map)); //Set kết quả trả về cho hàm giả lập tương ứng với map

Bước 4: Đối tượng cần test:
class DoiTuongCanTest
{
    protected $classGiaLap;

    public function __construct($hamGiaLap)
    {
        $this->classGiaLap = $classGiaLap;
    }

    public function hamCanTest($giaTri1, $giaTri2)
    {
        $ketQua1 = $this->classGiaLap->hamGiaLap($giaTri1);
        $ketQua2 = $this->classGiaLap->hamGiaLap($giaTri2);
        return [
            $ketQua1, $ketQua2
        ];
    }
}

Bước 4: Test
$doiTuongCanTest = new DoiTuongCanTest($classMocker);
$this->assertEquals($ketQuaMongMuon, $weather->hamCanTest(giáTri1TruyenVaoHam, giáTri2TruyenVaoHam));

=>> Cách 2: Dùng Mockery

public function testHamCanTest()
{
    $classMocker = Mockery::mock(DoiTuongGiaLap::class);
    $classMocker
        ->shouldReceive('hamGiaLap')
        ->once()
        ->with($giaTriTruyenVàoHamGiaLap1)
        ->andReturn($ketQuaSeTraVe);
    $classMocker
        ->shouldReceive('hamGiaLap')
        ->once()
        ->with($giaTriTruyenVàoHamGiaLap2)
        ->andReturn($ketQuaSeTraVe);

    $doiTuongCanTest = new DoiTuongCanTest($classMocker);
    $this->assertEquals($ketQuaMongMuon, $weather->hamCanTest(giáTri1TruyenVaoHam, giáTri2TruyenVaoHam));
}

============ Bài 28: Testing cùng spies ============

- Mục đích: kiểm tra xem điều gì xảy ra sau khi code và test được gọi
B1: Tạo đối tượng test
$classCanTest = new classCanTest();
B2: Assert
$this->assertEquals($ketQuaMongMuon, $ketQuaTraVe);
B3: Tạo spy
$spy = Mockery::spy('ClassGiaLap');
B4: Chạy hàm trong class cần test
$classCanTest->hamCanTest($spy);
B5: Config spy
$spy->shouldHaveReceived('hamGiaLap')->once()->with($giaTriTruyenVao);

============ Bài 29: Test driven: Setup project ============
- Mục đích: Dùng để check code hoạt động như mong đợi, tìm lỗi và fix lỗi.
- Các bước setup project để test driven development

Bước 1: Tạo project
composer require --dev phpunit/phpunit

Bước 2: Tạo thư mục:
 - PHPUNITCourse
    - src
    - tests
    - vendor(thư mục này tạo tự động bởi composer)
    - composer.json
    - composer.lock
    - phpunit.xml

Bước 3: Config cho file json
{
    "require": {
        "phpunit/phpunit": "^9.5"
    },
    "autoload": {
        "psr-4": {
            "App\\" : "./src" <---------- Name space là App
        }
    }
}

Bước 4: Cấu hình file phpunit.xml
<?xml version="1.0" encoding="UTF-8" ?>
<phpunit colors="true" verbose="true" bootstrap="./vendor/autoload.php">
    <testsuites>
        <testsuite name="Test suite">
            <directory>./test</directory> <------------- Chạy các file bên trong thư mục test
        </testsuite>
    </testsuites>
</phpunit>

Bước 5: Chạy command alias
alias phpunit='vendor/bin/phpunit'

============ Bài 30: Test title artical ============
Test mặc định title của artical class là empty
* File artical class
class Artical
{
    public $title = "";
}

* File test artical
use PHPUnit\Framework\TestCase;
use App\Artical;

class ArticalTest extends TestCase
{
    public function testEmptyArticalTitle()
    {
        $artical = new Artical;
        $this->assertEmpty($artical->title);
    }
}

============ Bài 31: Sử dụng assertSame ============
Vì assert equal không phân biệt được empty và null cho nên chúng tả sử dụng assertSame thay thế
Ví dụ:

*File artical class
namespace App;

class Artical
{
    public $title = "";

    public function getSlug()
    {
        
    }
}

*File test artical class
<?php

use PHPUnit\Framework\TestCase;
use App\Artical;

class ArticalTest extends TestCase
{
    public function testSlugEmptyWithNoTitle()
    {
        $artical = new Artical;
        $this->assertEqual($artical->getSlug(), "");
    }
}

AssertEqual sẽ pass qua cái so sánh
=> Cho nên trường hợp này dùng assertSame
public function testSlugEmptyWithNoTitle()
{
    $artical = new Artical;
    $this->assertSame($artical->getSlug(), "");
}

Kết quả: 
1) ArticalTest::testSlugEmptyWithNoTitle
Failed asserting that '' is identical to null.

Đi fix lỗi bằng cách return chuỗi cho hàm getSlug
* File artical
public function getSlug()
{
    return "";
}

============ Bài 32: Test slug gạch chân ============

- Bài này dùng hàm setUp để dựng đối tượng Artical vào class ArticalTest
- Hàm setUp sẽ được chạy trước mỗi khi chạy từng hàm test trong file ArticalTest

- Ví dụ bài test artical có trả về slug không:

* File artical
namespace App;

class Artical
{
    public $title = "";

    public function getSlug()
    {
        return str_replace(" ", "_", $this->title);
    }
}

* File test artical

use PHPUnit\Framework\TestCase;
use App\Artical;

class ArticalTest extends TestCase
{
    protected $artical = null;

    public function setUp() : void
    {
        $this->artical = new Artical;
    }

    public function testSlugIsUnderscore()
    {
        $this->artical->title = "hoc unittest";
        $this->assertEquals($this->artical->getSlug(), "hoc_unittest");
    }
}

Sau khi test sẽ pass qua

============ Bài 33: Test getSlug khi title đầu vào nhiều space và ký tự khác ============
- Trường hợp set giá trị title có nhiều space
- Sử dụng hàm: preg_replace thay thế cho str_replace
- Cách dùng:
$slug = preg_replace("/\s+/", "_", $title);
/ / : Bắt buộc phải mở và đóng
\s: 1 ký tự khoảng trắng
\s+: Các ký tự khoảng trắng

- Sử dụng hàm trim để bóp 2 đầu của chuỗi:
- Cách dùng:
$slug = trim($slug, "_");

============ Bài 34: Test slug không có ký tự đặc biệt ============
- Xóa bỏ ký tự đặc biệt
[^\w]: Xóa ký tự đặc biệt và khoảng trắng
public function getSlug()
{
    $slug = $this->title;
    $slug = preg_replace("/\s+/", "_", $slug);
    $slug = preg_replace("/[^\w]/", "", $slug);
    $slug = trim($slug, "_");
    return $slug;
}

B1: get title. Ví dụ:    hoc! unittest!   
B2: thay thế các ký tự khoảng trắng thành gạch dưới. KQ: _hoc!_unittest!_
B3: xóa ký tự đặc biệt. KQ: _hoc_unittest_
B4: xóa gạch ở đầu và cuối chuỗi. KQ: hoc_unittest

============ Bài 35: Test đồng loạt nhiều slug ============
- Lý do là không thể viết từng function để test cho mỗi trường hợp chuỗi đầu vào để
convert thành slug.
- Sử dụng provider trong unittest
- Cách dùng:

B1: Tạo hàm trả về mảng n phần tử tương đương n params truyền vào hàm test
VD:
public function titleProvider()
{
    return [
        ['hoc unittest', 'hoc_unittest'], //[0]: truyền vào, [1]: mong muốn
        ['hoc  unittest', 'hoc_unittest'],
        [' hoc unittest', 'hoc_unittest'],
        ['hoc! unittest!', 'hoc_unittest']
    ];
}

B2: Tạo hàm test
Lưu ý chỗ comment để mục đích sử dụng data do hàm đó cung cấp
/**
* @dataProvider titleProvider
* **/
public function testSlug($title, $slug)
{
    $this->artical->title = $title;
    $this->assertEquals($this->artical->getSlug(), $slug);
}